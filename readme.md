# Git Tutorial

## Description

Distributed source code tracking, version control, management system.

## Terminology
**Repo** - your project that's being tracked by git  
**Working Area** - The "space" where the files are not in the staging area  
**Staging Area** - The "space" where the files are ready to be committed  


## Common Commands

`git init` creates git in current directory
`git status` displays the files that are modified (different areas of git)  
`git add [.|file]` adds files to staging area.
`git commit -m "commit message"`

## Commit Strategies
